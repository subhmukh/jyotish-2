
- install visual studio code
- install extension Live Server v5.7.4 by Ritwick Dey
- install git command line tool
- navigate to parent folder (example PROJECT_FOLDER)

```
git clone git clone git@bitbucket.org:pankajupreti/jyotish.git

ls
   # you should see folder jyotish created

cd jyotish

git branch
  # you should see branch main

git checkout -b subhash

git branch
  # you should see branch subhash active
```

- open this folder jyotish in visual studio code
- from bottom right hand footer of VS Code click "Go Live" - this should open a browser window and you will see the test project
- click run and you will see result of your code

- do following to commit your changes

```
git add .
git commit -m "comment about what you are adding"
git push
```